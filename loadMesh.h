//
// Created by ido on 12/30/18.
//

#ifndef CHOPPER_LOADMESH_H
#define CHOPPER_LOADMESH_H

#include <string>
#include <igl/pathinfo.h>
#include <igl/readOBJ.h>
#include <igl/readOFF.h>
#include <igl/pathinfo.h>
#include <igl/readOBJ.h>
#include <igl/readOFF.h>
#include <igl/readMESH.h>
#include <igl/faces_first.h>
#include <igl/readTGF.h>
#include <igl/launch_medit.h>
#include <igl/boundary_conditions.h>
#include <igl/writeDMAT.h>
#include <igl/writeMESH.h>
#include <igl/normalize_row_sums.h>
#include <igl/bbw.h>

// Read a surface mesh from a {.obj|.off|.mesh} files
// Inputs:
//   mesh_filename  path to {.obj|.off|.mesh} file
// Outputs:
//   V  #V by 3 list of mesh vertex positions
//   F  #F by 3 list of triangle indices
// Returns true only if successfuly able to read file
bool load_mesh_from_file(
        const std::string mesh_filename,
        Eigen::MatrixXd & V,
        Eigen::MatrixXi & F)
{
    using namespace std;
    using namespace igl;
    using namespace Eigen;
    string dirname, basename, extension, filename;
    pathinfo(mesh_filename,dirname,basename,extension,filename);
    transform(extension.begin(), extension.end(), extension.begin(), ::tolower);
    bool success = false;
    if(extension == "obj")
    {
        success = readOBJ(mesh_filename,V,F);
    }else if(extension == "off")
    {
        success = readOFF(mesh_filename,V,F);
    }else if(extension == "mesh")
    {
        // Unused Tets read from .mesh file
        MatrixXi Tets;
        success = readMESH(mesh_filename,V,Tets,F);
        // We're not going to use any input tets. Only the surface
        if(Tets.size() > 0 && F.size() == 0)
        {
            // If Tets read, but no faces then use surface of tet volume
        }else
        {
            // Rearrange vertices so that faces come first
            VectorXi IM;
            faces_first(V,F,IM);
            // Dont' bother reordering Tets, but this is how one would:
            //Tets =
            //  Tets.unaryExpr(bind1st(mem_fun( static_cast<VectorXi::Scalar&
            //  (VectorXi::*)(VectorXi::Index)>(&VectorXi::operator())),
            //  &IM)).eval();
            // Don't throw away any interior vertices, since user may want weights
            // there
        }
    }else
    {
        cerr<<"Error: Unknown shape file format extension: ."<<extension<<endl;
        return false;
    }
    return success;
}


// Writes output files to /path/to/input/mesh-skeleton.dmat,
// mesh-volume.dmat, mesh-volume.mesh if input mesh was
// located at /path/to/input/mesh.obj and input skeleton was at
// /other/path/to/input/skel.tgf
//
// Writes:
////   mesh.dmat  dense weights matrix corresponding to original input
////     vertices V
//   mesh-volume.dmat  dense weights matrix corresponding to all
//     vertices in tet mesh used for computation VV
//   mesh-volume.mesh  Tet mesh used for computation
//
// Inputs:
//   mesh_filename  path to {.obj|.off|.mesh} file
//   skel_filename  path to skeleton {.bf|.tgf} file
//   V  #V by 3 list of original mesh vertex positions
//   F  #F by 3 list of original triangle indices
//   VV  #VV by 3 list of tet-mesh vertex positions
//   TT  #TT by 4 list of tetrahedra indices
//   FF  #FF by 3 list of surface triangle indices
//   W   #VV by #W weights matrix
// Returns true on success
bool save_output(
        const std::string mesh_filename,
        const std::string /*skel_filename*/,
        const Eigen::MatrixXd & V,
        const Eigen::MatrixXi & /*F*/,
        const Eigen::MatrixXd & VV,
        const Eigen::MatrixXi & TT,
        const Eigen::MatrixXi & FF,
        const Eigen::MatrixXd & W)
{
    using namespace std;
    using namespace igl;
    using namespace Eigen;
    // build filename prefix out of input base names
    string prefix = "";
    {
        string dirname, basename, extension, filename;
        pathinfo(mesh_filename,dirname,basename,extension,filename);
        transform(extension.begin(), extension.end(), extension.begin(), ::tolower);
        prefix += dirname + "/" + filename;
    }

    //{
    //  string dirname, basename, extension, filename;
    //  pathinfo(skel_filename,dirname,basename,extension,filename);
    //  transform(extension.begin(), extension.end(), extension.begin(), ::tolower);
    //  prefix += "-" + filename;
    //}

    // Keep track if any fail
    bool success = true;
    //// Weights matrix for just V. Assumes V prefaces VV
    //MatrixXd WV = W.block(0,0,V.rows(),W.cols());
    //// write dmat
    //success &= writeDMAT(prefix + ".dmat",WV);
    // write volume weights dmat
    success &= writeDMAT(prefix + "-volume.dmat",W);
    // write volume mesh
    success &= writeMESH(prefix + "-volume.mesh",VV,TT,FF);
    //// write surface OBJ with pseudocolor
    return success;
}

#endif //CHOPPER_LOADMESH_H
