//
// Created by ido on 12/30/18.
//

#ifndef CHOPPER_MESHMATRIXCONVERTER_H
#define CHOPPER_MESHMATRIXCONVERTER_H

#include <igl/pathinfo.h>
#include <vector>
#include "bsptree.hpp"
#include <boost/qvm/vec.hpp>
#include <boost/qvm/vec_operations.hpp>
#include <boost/qvm/mat_operations.hpp>
#include <boost/qvm/map_vec_mat.hpp>
#include <boost/qvm/swizzle.hpp>
#include <boost/qvm/vec_mat_operations.hpp>
#include <boost/qvm/mat.hpp>
#include <igl/opengl/glfw/Viewer.h>

using namespace boost::qvm;

struct Vertex {
    vec<float, 3> pos;
};

void convertToIglFormat(
        const std::vector<Vertex> &vertices,
        const std::vector<int> &indcies,
        Eigen::MatrixXd &V,
        Eigen::MatrixXi &F) {
    V.resize(vertices.size(), 3);
    int i = 0;
    for (Vertex vertex : vertices) {
        V(i, 0) = vertex.pos.a[0];
        V(i, 1) = vertex.pos.a[1];
        V(i, 2) = vertex.pos.a[2];
        i++;
    }

    F.resize(indcies.size() / 3, 3);
    i = 0;
    int j = 0;
    for (int index : indcies) {
        F(i, j) = index;
        j = ++j % 3;
        if (j == 0) {
            i++;
        }
    }

}

bool convertToBspFormat(
        const Eigen::MatrixXd &V,
        const Eigen::MatrixXi &F,
        std::vector<Vertex> &vertices,
        std::vector<int> &indcies) {
    vertices.resize(V.rows());
    for (int i = 0; i < V.rows(); ++i) {
        Vertex vertex;
        for (int j = 0; j < V.cols(); ++j) {
            vertex.pos.a[j] = V(i,j);
        }
        vertices[i] = vertex;
    }

    indcies.resize(F.rows()*F.cols());
    for (int i = 0; i < F.rows(); ++i) {
        for (int j = 0; j < F.cols(); ++j) {
            indcies[i*F.cols() + j] = F(i,j);
        }
    }
}


#endif //CHOPPER_MESHMATRIXCONVERTER_H
