#include "bsptree.hpp"
#include "stl.hpp"
#include "loadMesh.h"
#include "meshMatrixConverter.h"

#include <boost/qvm/vec.hpp>
#include <boost/qvm/vec_operations.hpp>
#include <boost/qvm/mat_operations.hpp>
#include <boost/qvm/map_vec_mat.hpp>
#include <boost/qvm/swizzle.hpp>
#include <boost/qvm/vec_mat_operations.hpp>
#include <boost/qvm/mat.hpp>

#include <vector>
#include <iostream>

#include <igl/opengl/glfw/Viewer.h>
#include <igl/bounding_box.h>

using namespace boost::qvm;

namespace bsp {
    template<>
    struct vertex_traits<Vertex> {
        typedef vec<float, 3> position_type;

        static const position_type &getPosition(const Vertex &v) {
            return v.pos;
        }

        static Vertex getInterpolatedVertex(const Vertex &a, const Vertex &b, float i) {
            return {a.pos * (1 - i) + b.pos * i};
        }

        // because we use the transform functionality, we need to add this
        // function to the trait
        static void transform(Vertex &v, const boost::qvm::mat<float, 4, 4> &m) {
            boost::qvm::vec<float, 4> pp = boost::qvm::XYZ1(v.pos);

            pp = m * pp;
            pp /= boost::qvm::W(pp);
            v.pos = boost::qvm::XYZ(pp);
        }
    };
}

int main() {
    using namespace std;
    using namespace Eigen;
    using namespace igl;
    string mesh_filename = "/home/ido/Downloads/ogre/bs_smile.obj";

    // #V by 3 list of mesh vertex positions
    MatrixXd V;
    // #F by 3 list of triangle indices
    MatrixXi F;
    // load mesh from .obj, .off or .mesh
    if(!load_mesh_from_file(mesh_filename,V,F))
    {
        return 1;
    }

    std::vector<Vertex> v;
    std::vector<int> indices;

    convertToBspFormat(V,F,v, indices);


//    std::vector<Vertex> v {
//            {0, 0, 1},{1, 0, 1},{1, 1, 1},    {0, 0, 1}, {1, 1, 1}, {0, 1, 1},
//            {0, 0, 0},{1, 1, 0},{1, 0, 0},    {0, 0, 0}, {0, 1, 0}, {1, 1, 0},
//
//            {0, 1, 0},{1, 1, 1},{1, 1, 0},    {0, 1, 0}, {0, 1, 1}, {1, 1, 1},
//            {0, 0, 0},{1, 0, 0},{1, 0, 1},    {0, 0, 0}, {1, 0, 1}, {0, 0, 1},
//
//            {1, 0, 0},{1, 1, 0},{1, 1, 1},    {1, 0, 0}, {1, 1, 1}, {1, 0, 1},
//            {0, 0, 0},{0, 1, 1},{0, 1, 0},    {0, 0, 0}, {0, 0, 1}, {0, 1, 1},
//    };
//
//    std::vector<int> indices(v.size());
//
//    for (int k = 0; k < v.size(); ++k) {
//        indices[k] = k;
//
//    }
//
    bsp::BspTree<std::vector<Vertex>, std::vector<int>,4, double > bsp3(std::move(v), std::move(indices));


    std::tuple<boost::qvm::vec<double , 3>, double > plane{
            {-1000, -1000, -1000}, 0
    };

    for (auto &myPair : bsp3.nodeToIndex) {
        bsp3.makeCut(myPair.first, myPair.second, plane);
    }

    for (auto &myPair : bsp3.nodeToIndex) {
        std::cout << myPair.second.size() << std::endl;
    }
//
//
//    std::cout << "sdfsdf" << std::endl;
//    Eigen::MatrixXd V;
//    Eigen::MatrixXi F;
//    std::vector<int> &second = (bsp3.nodeToIndex.begin())->second;
//
//    convertToIglFormat(bsp3.vertices_, second, V, F);

//    Eigen::MatrixXd V(bsp3.vertices_.size(), 3);
//    int i = 0;
//    for (Vertex vertex : bsp3.vertices_) {
//        V(i,0) = vertex.pos.a[0];
//        V(i,1) = vertex.pos.a[1];
//        V(i,2) = vertex.pos.a[2];
//        i++;
//    }
//
//    std::cout << "v: " << V << std::endl;
//    std::vector<int> &second = (bsp3.nodeToIndex.begin())->second;
//    Eigen::MatrixXi behind(second.size() / 3, 3);
//    int ii = 0;
//    int jj = 0;
//    for (int index : second) {
//        behind(ii,jj) = index;
//        jj = ++jj % 3;
//        if (jj == 0) {
//        ii++;
//        }
//    }




//    const Eigen::MatrixXd V = (Eigen::MatrixXd(8, 3) <<
//                                                     0.0, 0.0, 0.0,
//            0.0, 0.0, 1.0,
//            0.0, 1.0, 0.0,
//            0.0, 1.0, 1.0,
//            1.0, 0.0, 0.0,
//            1.0, 0.0, 1.0,
//            1.0, 1.0, 0.0,
//            1.0, 1.0, 1.0).finished();
//    const Eigen::MatrixXi F = (Eigen::MatrixXi(12, 3) <<
//                                                      1, 7, 5,
//            1, 3, 7,
//            1, 4, 3,
//            1, 2, 4,
//            3, 8, 7,
//            3, 4, 8,
//            5, 7, 8,
//            5, 8, 6,
//            1, 5, 6,
//            1, 6, 2,
//            2, 6, 8,
//            2, 8, 4).finished().array() - 1;
//
//    std::vector<Vertex> v;
//
//    std::vector<int> indices;
//
//    convertToBspFormat(V,F,v, indices);

    std::vector<int> &second = (bsp3.nodeToIndex.begin())->second;
    Eigen::MatrixXd VV;
    Eigen::MatrixXi FF;
    convertToIglFormat(bsp3.vertices_,second,VV,FF);

    // Plot the mesh
    igl::opengl::glfw::Viewer viewer;
    viewer.data().set_mesh(V, F);
    viewer.data().set_face_based(true);
    viewer.launch();


    //------------bounding box---------------


    MatrixXd BV;
    MatrixXi BF;
    bounding_box<Matrix<double, Eigen::Dynamic, Eigen::Dynamic>,
            Matrix<double, Eigen::Dynamic, Eigen::Dynamic>,
            Matrix<int, Eigen::Dynamic, Eigen::Dynamic>>(V, BV, BF);



    //----------------------------------------



//    auto a3 = bsp3.sort(vec<float, 3>{-5, 5, 5});
//
//    if (bsp3.isInside(vec<float, 3>{-5, 5, 5})) printf("oops 1\n");
//    if (!bsp3.isInside(vec<float, 3>{0.5, 0.5, 0.5})) printf("oops 3\n");
//
//    printSTL(bsp3.getVertices(), bsp3.sort(vec<float, 3>{-5, 5, 5}));
}